package br.com.cit.sms.enums;

import br.com.cit.sms.utils.Constantes;
import br.com.cit.sms.utils.SmsException;

public enum OpcaoSmsEnum {

	CODIFICAR(1, "Codificar Mensagem"),
	DECODIFICAR(2, "Decodificar Mensagem");
	
	private int code;
	
	private String description;
	
	OpcaoSmsEnum(int code, String description) {
		this.code = code;
		this.description = description;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the code
	 */
	public int getCode() {
		return code;
	}

	/**
	 * @param code the code to set
	 */
	public void setCode(int code) {
		this.code = code;
	}

	public static OpcaoSmsEnum parse(String codigo) throws SmsException {
		OpcaoSmsEnum retorno;
		switch (codigo) {
		case "1":
			retorno = CODIFICAR;
			break;
		case "2":
			retorno = DECODIFICAR;
			break;
		default:
			throw new SmsException(Constantes.ARGUMENTO_INVALIDO);
		}
		return retorno;
	}
	
}
