package br.com.cit.sms.interfaces;

import br.com.cit.sms.enums.OpcaoSmsEnum;
import br.com.cit.sms.utils.SmsException;

public interface Logging {

	void logAcao(String usuario, OpcaoSmsEnum opcao) throws SmsException;
	
	void logErro(String mensagem) throws SmsException;

	void logAutenticacao(String usuario, boolean sucesso) throws SmsException;
	
}
