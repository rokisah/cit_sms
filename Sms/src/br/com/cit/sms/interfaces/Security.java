package br.com.cit.sms.interfaces;

import br.com.cit.sms.utils.SmsException;

public interface Security {

	boolean dologin(String usuario, String senha, Logging logger) throws SmsException;
}
