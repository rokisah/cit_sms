package br.com.cit.sms.interfaces;

import br.com.cit.sms.enums.OpcaoSmsEnum;
import br.com.cit.sms.utils.SmsException;

public interface Message {

	public String processar(OpcaoSmsEnum opcao, String mensagem) throws SmsException;
	
}
