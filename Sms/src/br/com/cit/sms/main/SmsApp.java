package br.com.cit.sms.main;

import br.com.cit.sms.business.LoggingTxt;
import br.com.cit.sms.business.MessageGsm;
import br.com.cit.sms.business.SecurityMock;
import br.com.cit.sms.enums.OpcaoSmsEnum;
import br.com.cit.sms.interfaces.Logging;
import br.com.cit.sms.interfaces.Message;
import br.com.cit.sms.interfaces.Security;
import br.com.cit.sms.utils.Constantes;
import br.com.cit.sms.utils.SmsException;

public class SmsApp {

	private String usuario;
	
	private String senha;
	
	private String mensagem;
	
	private OpcaoSmsEnum opcao;
	
	private String mensagemRetorno;
	
	public static void main(String[] args) throws SmsException {
		try {
			SmsApp sms = new SmsApp();
			sms.inicializar(args);
			sms.autenticar();
			sms.processar();
			System.out.println(sms.getMensagemRetorno());
		} catch (SmsException e) {
			Logging logger = new LoggingTxt();
			logger.logErro(e.getMensagem());
			System.out.println(e.getMensagem());
		}

	}

	public void inicializar(String[] args) throws SmsException {
		if (args.length != 4) {
			throw new SmsException(Constantes.ARGUMENTO_INVALIDO);
		}
		this.usuario = args[0];
		this.senha = args[1];
		this.opcao = OpcaoSmsEnum.parse(args[2]);
		this.mensagem = args[3]; 
	}

	public void autenticar() throws SmsException {
		Security security = new SecurityMock();
		Logging logger = new LoggingTxt();
		if (!security.dologin(this.usuario, this.senha, logger)) {
			throw new SmsException(Constantes.FALHA_LOGIN);
		}
	}
	
	public void processar() throws SmsException {
		Message message = new MessageGsm();
		this.mensagemRetorno = message.processar(this.opcao, this.mensagem);
		Logging logger = new LoggingTxt();
		logger.logAcao(this.usuario, this.opcao);
	}
	
	/**
	 * @return the usuario
	 */
	public String getUsuario() {
		return usuario;
	}

	/**
	 * @param usuario the usuario to set
	 */
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	/**
	 * @return the senha
	 */
	public String getSenha() {
		return senha;
	}

	/**
	 * @param senha the senha to set
	 */
	public void setSenha(String senha) {
		this.senha = senha;
	}

	/**
	 * @return the mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}

	/**
	 * @param mensagem the mensagem to set
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	/**
	 * @return the opcao
	 */
	public OpcaoSmsEnum getOpcao() {
		return opcao;
	}

	/**
	 * @param opcao the opcao to set
	 */
	public void setOpcao(OpcaoSmsEnum opcao) {
		this.opcao = opcao;
	}

	/**
	 * @return the mensagemRetorno
	 */
	public String getMensagemRetorno() {
		return mensagemRetorno;
	}

	/**
	 * @param mensagemRetorno the mensagemRetorno to set
	 */
	public void setMensagemRetorno(String mensagemRetorno) {
		this.mensagemRetorno = mensagemRetorno;
	}

}
