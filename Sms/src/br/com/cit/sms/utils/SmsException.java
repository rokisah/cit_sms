package br.com.cit.sms.utils;

public class SmsException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6758481850457945671L;

	private String mensagem;

	/**
	 * @return the mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}

	public SmsException(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * @param mensagem the mensagem to set
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
}
