package br.com.cit.sms.utils;


public class Constantes {

	public static final String ALFABETO = "abcdefghijklmnopqrstuvwxyz";
	public static final String ARGUMENTO_INVALIDO = "Argumento inválido";
	public static final String FALHA_LOGIN = "Credenciais inválidas";
	public static final String LOG_ACAO = "Usuario {0} executou acao {1}";
	public static final String LOG_ERRO = "Erro de sistema: {0}";
	public static final String LOG_AUTENTICACAO_SUCESSO = "Usuario {0} autenticado com sucesso";
	public static final String LOG_AUTENTICACAO_FALHA = "Tentativa de acesso nao autorizado. usuario: {0}";
	public static final String CAMINHO_LOG_TXT = "C:\\SmsLog.txt";
	
}
