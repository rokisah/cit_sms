package br.com.cit.sms.business;

import java.text.MessageFormat;

import br.com.cit.sms.enums.OpcaoSmsEnum;
import br.com.cit.sms.interfaces.Logging;
import br.com.cit.sms.utils.Constantes;
import br.com.cit.sms.utils.SmsException;

public abstract class LoggingBase implements Logging {

	@Override
	public void logAcao(String usuario, OpcaoSmsEnum opcao) throws SmsException {
		String log = MessageFormat.format(Constantes.LOG_ACAO, usuario, opcao.getDescription());
		gravar(log);
	}

	@Override
	public void logErro(String mensagem) throws SmsException {
		String log = MessageFormat.format(Constantes.LOG_ERRO, mensagem);
		gravar(log);		
	}

	@Override
	public void logAutenticacao(String usuario, boolean sucesso) throws SmsException {
		String log;
		if (sucesso) {
			log = MessageFormat.format(Constantes.LOG_AUTENTICACAO_SUCESSO, usuario);
		} else {
			log = MessageFormat.format(Constantes.LOG_AUTENTICACAO_FALHA, usuario);
		}
		gravar(log);		
	}

	protected abstract void gravar(String log) throws SmsException;

}
