package br.com.cit.sms.business;

import br.com.cit.sms.interfaces.Logging;
import br.com.cit.sms.interfaces.Message;
import br.com.cit.sms.utils.Constantes;
import br.com.cit.sms.utils.SmsException;

public abstract class MessageBase implements Message {

	protected String codificar(String mensagem) {
		StringBuilder sbRetorno = new StringBuilder();
		StringBuilder sbNumero = new StringBuilder();
		for (int i = 0; i < mensagem.length() - 1; i++) {
			char atual = mensagem.charAt(i);
			char proxima = mensagem.charAt(i + 1);
			sbNumero.append(atual);
			if (atual != proxima) {
				sbRetorno.append(this.codificarNumero(sbNumero.toString()));
				sbNumero = new StringBuilder();
				if (proxima == '_') {
					i++;
				}
			}
		}
		char atual = mensagem.charAt(mensagem.length() - 1);
		sbNumero.append(atual);
		sbRetorno.append(this.codificarNumero(sbNumero.toString()));
		return sbRetorno.toString();
	}
	
	protected String decodificar(String mensagem) throws SmsException {
		StringBuilder sbRetorno = new StringBuilder();
		for (char letra : mensagem.toLowerCase().toCharArray()) {
			String retorno = this.decodificarLetra(letra);
			if (sbRetorno.toString().length() > 1) {
				char anterior = sbRetorno.toString().charAt(sbRetorno.toString().length() - 1);
				char seguinte = retorno.charAt(0);
				if (anterior == seguinte) {
					sbRetorno.append('_');
				}
			}
			sbRetorno.append(retorno);
		}
		return sbRetorno.toString();
	}
	
	/**
	 * Coment�rio do autor
	 * M�todo com o uso de um algoritmo complexo
	 * Vantagem: c�digo mais curto
	 * Rotina que atende a todos os caracteres,
	 * sem a necessidade de testar um a um.
	 * mais "bonito"
	 * Desvantagem: complexo para dar manuten��o
	 * @param numeroEntrada
	 * @return
	 */
	private char codificarNumero(String numeroEntrada) {
		char retorno;
		if (numeroEntrada.equals("0")) {
			retorno = ' ';
		} else {
			int numero = Integer.parseInt(String.valueOf(numeroEntrada.charAt(0)));
			numero = numero - 2;
			numero = numero * 3;
			if (numero > 17) {
				numero = numero + 1;
			}
			int qtd = numeroEntrada.length();
			numero = numero + qtd - 1;
			retorno = Constantes.ALFABETO.charAt(numero);
		}
		return retorno;
	}
	
	/**
	 * Coment�rio do autor
	 * M�todo sem uso de algor�tmo
	 * Vantagem: mais f�cil de criar
	 * e mais f�cil de dar manuten��o
	 * Desvantagem: c�digo muito extenso e "feio"
	 * @param letra
	 * @return
	 * @throws SmsException 
	 */
	private String decodificarLetra(char letra) throws SmsException {
		String retorno;
		switch (letra) {
		case 'a':
			retorno = "2";
			break;
		case 'b':
			retorno = "22";
			break;
		case 'c':
			retorno = "222";
			break;
		case 'd':
			retorno = "3";
			break;
		case 'e':
			retorno = "33";
			break;
		case 'f':
			retorno = "333";
			break;
		case 'g':
			retorno = "4";
			break;
		case 'h':
			retorno = "44";
			break;
		case 'i':
			retorno = "444";
			break;
		case 'j':
			retorno = "5";
			break;
		case 'k':
			retorno = "55";
			break;
		case 'l':
			retorno = "555";
			break;
		case 'm':
			retorno = "6";
			break;
		case 'n':
			retorno = "66";
			break;
		case 'o':
			retorno = "666";
			break;
		case 'p':
			retorno = "7";
			break;
		case 'q':
			retorno = "77";
			break;
		case 'r':
			retorno = "777";
			break;
		case 's':
			retorno = "7777";
			break;
		case 't':
			retorno = "8";
			break;
		case 'u':
			retorno = "88";
			break;
		case 'v':
			retorno = "888";
			break;
		case 'w':
			retorno = "9";
			break;
		case 'x':
			retorno = "99";
			break;
		case 'y':
			retorno = "999";
			break;
		case 'z':
			retorno = "9999";
			break;
		case ' ':
			retorno = "0";
			break;
		default:
			throw new SmsException(Constantes.ARGUMENTO_INVALIDO);		}
		return retorno;
	}

	
}
