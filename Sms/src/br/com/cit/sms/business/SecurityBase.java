package br.com.cit.sms.business;

import br.com.cit.sms.interfaces.Logging;
import br.com.cit.sms.interfaces.Security;
import br.com.cit.sms.utils.SmsException;

public abstract class SecurityBase implements Security {

	@Override
	public boolean dologin(String usuario, String senha, Logging logger) throws SmsException {
		boolean sucesso = Boolean.FALSE;
		if (autenticar(usuario, senha)) {
			sucesso = Boolean.TRUE;
			logger.logAutenticacao(usuario, sucesso);
		}
		return sucesso;
	}

	protected abstract boolean autenticar(String usuario, String senha);

}
