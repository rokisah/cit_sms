package br.com.cit.sms.business;

import br.com.cit.sms.enums.OpcaoSmsEnum;
import br.com.cit.sms.utils.Constantes;
import br.com.cit.sms.utils.SmsException;

public class MessageGsm extends MessageBase {

	public String processar(OpcaoSmsEnum opcao, String mensagem) throws SmsException {
		String retorno = "";
		switch (opcao) {
		case CODIFICAR:
			retorno = this.codificar(mensagem);
			break;
		case DECODIFICAR:
			retorno = this.decodificar(mensagem);
			break;
		default:
			throw new SmsException(Constantes.ARGUMENTO_INVALIDO);
		}
		return retorno;
	}

}
