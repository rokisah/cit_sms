package br.com.cit.sms.business;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

import br.com.cit.sms.utils.Constantes;
import br.com.cit.sms.utils.SmsException;


public class LoggingTxt extends LoggingBase {

	@Override
	protected void gravar(String log) throws SmsException {
		try {
			Files.write(Paths.get(Constantes.CAMINHO_LOG_TXT), (log + System.lineSeparator()).getBytes(),
					StandardOpenOption.CREATE, StandardOpenOption.APPEND);
		} catch (IOException e) {
			throw new SmsException(e.getMessage());
		}
		
	}

}
